const messages = require('./messages/messages.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  const options = {
    Model: messages,
    paginate: {
      default: 50,
      max:100
    }
  }
  app.configure(messages);
};
